#Mac Swing Native OSX Menus
Use Substance Look and Feel with native OS X menus

![Screen demo][3]

* From  [StackOverflow][1]
* Using [Substance Look and Feel][2]

[1]:http://stackoverflow.com/questions/5688518/substance-and-macos-menubar "Stack Overflow"
[2]:[http://insubstantial.github.com/insubstantial/substance/ "Substance Look and Feel"
[3]:https://bitbucket.org/cunneen/macswinglafmenu/raw/master/screenshot.png "Screen shot"